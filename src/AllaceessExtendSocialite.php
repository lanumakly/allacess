<?php

namespace SocialiteProviders\Allaccess;

use SocialiteProviders\Allaccess\Provider;
use SocialiteProviders\Manager\SocialiteWasCalled;

class AllacessExtendSocialite
{
    public function handle(SocialiteWasCalled $socialiteWasCalled)
    {
        $socialiteWasCalled->extendSocialite('allaccess', Provider::class);
    }
}