<?php

namespace SocialiteProviders\Allaccess;

use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\ProviderInterface;
use Laravel\Socialite\Two\User;
// use SocialiteProviders\Manager\

class Provider extends AbstractProvider implements ProviderInterface
{
    /**
     * Unique Provider Identifier.
     */
    public const IDENTIFIER = 'ALLACCESS';

    protected $name = "ssoqa-kly";

    protected $secret_key = "5665111e380bd149203424f077f3b256b058cef1161171af316a32b4";

    protected $base_url = 'https://qa.allaccess.id/api';

    protected $clientId = "451c5d46f893e8416e10318033efd4df-nyJKxgy4XhuCBrvU8CEkgJFTgEd7CBzJ.client.allaccess.id";

    protected $client_name = "KLY_amild_20200930";

    protected $client_key = "6ea8d7e324b4a3c0dd0422ecc96bf783-tPcMoodYJ2oco9tPgiBf0NPIUoWvfn2g";

    protected $redirect_url = "https://ssoallacess.test/";

    protected $platform = "amild x kly";

    /**
     * @return array
     */
    public static function additionalConfigKeys()
    {
        return [
            'url',
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase('/auth/request-url', $state);
    }

    /**
     * {@inheritdoc}
     */
    protected function buildAuthUrlFromBase($url, $state)
    {
        return $url.'?'.http_build_query(
            [
                'client_id'     => $this->clientId,
                'client_name'   => $this->client_name,
                'client_key'    => $this->client_key,
                'redirect_url'  => $this->redirect_url,
                'platform'      => $this->platform,
            ],
            '',
            '&',
            $this->encodingType
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenUrl()
    {
        return $this->base_url . '/token/get';
    }

    /**
     * {@inheritdoc}
     */
    public function getAccessToken($code)
    {
        $response = $this->getHttpClient()->post($this->getTokenUrl(), [
            'headers'     => ['Accept' => 'application/json'],
            'form_params'    => $this->getTokenFields($code),
        ]);

        $data = json_decode($response->getBody()->getContents(), true);

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenFields($code)
    {
        return [
            'name' => $this->name,
            'secret_key' => $this->secret_key
        ];
    }

    /**
     * Set the redirect URL.
     *
     * @param  string  $url
     * @return $this
     */
    public function redirectUrl($url)
    {
        $this->redirectUrl = $url;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function getUserByToken($token)
    {
        return ;
    }

    /**
     * {@inheritdoc}
     */
    protected function mapUserToObject(array $user)
    {
        return ;
    }
}